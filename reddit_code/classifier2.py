import os
import numpy as np
import pandas as pd
import tensorflow as tf
import json
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
from keras.optimizers import SGD, Adam, RMSprop
from sklearn.model_selection import KFold, cross_val_score
from keras.utils import to_categorical
from scipy.sparse import csr_matrix
from sklearn.preprocessing import OneHotEncoder
from file_operation import list_files, sort_files_list

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


# file_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/10_authors/RC_2009-01_10k_authors.csv.gz'

# df = pd.read_table(file_path, encoding='utf-8', compression='gzip', usecols=['words_feat', 'author_id'])

# def batch_generator(X_data, y_data, batch_size):
#     samples_per_epoch = X_data.shape[0]
#     number_of_batches = samples_per_epoch/batch_size
#     counter=0
#     index = np.arange(np.shape(y_data)[0])
#     while 1:
#         index_batch = index[batch_size*counter:batch_size*(counter+1)]
#         X_batch = X_data[index_batch,:]
#         y_batch = y_data[index_batch]
#         counter += 1
#         yield np.array(X_batch), y_batch
#         if (counter > number_of_batches):
#             counter=0


# train, validate, test = np.split(df.sample(frac=1, random_state=42),
#                                  [int(.6*len(df)), int(.8*len(df))])

# authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv'

def authorsIdEncoder(authors_list_path, authors_num):
    encoder = OneHotEncoder()
    authors_df = pd.read_csv(authors_list_path, nrows=authors_num)
    authors_id = authors_df['id'].values
    encoder.fit(authors_id.reshape(-1, 1))
    return encoder

def extract_json(json_row):
    return json.loads(json_row)

def extract_feature(inputs, feature_type):
    json_inputs = [extract_json(row) for row in inputs]
    # return [row['lowercase'][feature_type] for row in json_inputs]
    return [row['commom_case'][feature_type] for row in json_inputs]

def build_model(targets_num, hidden_num, max_seq):
    model = tf.keras.Sequential()
    tensor_input = model.add(tf.keras.layers.Input(shape=[None, max_seq], batch_size=5000, dtype=tf.float32, ragged=True))
    # model.add(tf.keras.layers.LSTM(hidden_num, return_sequences=True))
    # model.add(tf.keras.layers.Dropout(0.2))
    model.add(tf.keras.layers.LSTM(hidden_num))
    model.add(tf.keras.layers.Dense(targets_num, activation='softmax'))
    opt = RMSprop(learning_rate=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    model.summary()
    return model


def generate_model(result_path, dataset_path, authors_list_path, authors_num, chunksize,
                   feature_type, epoches, hidden_num, verbose):
    monthly_files = sort_files_list(list_files(dataset_path, '*.csv'))
    y_encoder = authorsIdEncoder(authors_list_path, authors_num)
    model = build_model(authors_num, hidden_num, 1)
    model_loss = []
    model_acc = []
    model_history = {
        'loss': np.zeros(epoches),
        'accuracy': np.zeros(epoches)
    }
    for epoch in range(epoches):
        print('Epoch: ' + str(epoch))
        tmp_loss = np.array([])
        tmp_accuracy = np.array([])
        for file in monthly_files:
            # print('Train: ' + file)
            for df in pd.read_table(file, chunksize=chunksize, lineterminator='\n'):
                inputs = extract_feature(df['words_feat'].values, feature_type)
                x = [np.array(xi).reshape(-1, 1) for xi in inputs]
                y = y_encoder.transform(df['author_id'].values.reshape(-1,1)).toarray()
                X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=100)
                X_train = tf.ragged.constant(X_train)
                X_test = tf.ragged.constant(X_test)
                # history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=1, verbose=verbose)
                # model_history['loss'][epoch] += history.history['loss']
                # model_history['accuracy'][epoch] += history.history['accuracy']
                # print()
                # model_history.append(history)
                #
                # or
                #
                # for i in range(epoches):
                #     print('Batch ' + str(i))
                model.train_on_batch(X_train, y_train)
                history = model.test_on_batch(X_test, y_test, return_dict=True)
                # print('Loss: ' + str(history))
                tmp_loss = np.append(tmp_loss, history['loss'])
                tmp_accuracy = np.append(tmp_accuracy, history['accuracy'])
                # model_history['loss'][epoch] += history['loss']
                # model_history['accuracy'][epoch] += history['accuracy']
        model_history['loss'][epoch] = np.mean(tmp_loss)
        model_history['accuracy'][epoch] = np.mean(tmp_accuracy)
    del model
    accuracy_file_path = f'{result_path}/accuracy_{authors_num}_authors_{feature_type}_{hidden_num}_hidden.jpg'
    loss_file_path = f'{result_path}/loss_{authors_num}_authors_{feature_type}_{hidden_num}_hidden.jpg'
    savePlot(range(len(model_history['accuracy'])), model_history['accuracy'],
            'epoches', 'accuracy', accuracy_file_path)
    savePlot(range(len(model_history['loss'])), model_history['loss'],
        'epoches', 'loss', loss_file_path)
    return model_history


def savePlot(x, y, x_label, y_label, file_path):
    plt.plot(x, y)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(file_path)
    plt.close()


dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/10_authors_1000_comments/'
authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv'
result_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/Experimento/results/tests'

model_history = generate_model(result_path, dataset_path, authors_list_path, 10, 100, 'trigram', 50, 100, True)

model_history = generate_model(result_path, dataset_path, authors_list_path, 10, 30000, 'unigram', 10, 200, True)

# plt.plot(range(len(model_history['accuracy'])), model_history['accuracy'])
# plt.ylabel('epoches')
# plt.ylabel('accuracy')
# plt.show()

# plt.plot(range(len(model_history['loss'])), model_history['loss'])
# plt.ylabel('epoches')
# plt.ylabel('loss')
# plt.show()


inputs = extract_unigram(df['words_feat'].values)
# features = 1
# x = [np.array(xi).reshape(-1, 1) for xi in inputs].reshape((1500,-1,1))
# x = np.array(inputs).reshape((1500,1,1))
x = [np.array(xi).reshape(-1, 1) for xi in inputs]

targets_num = len(np.unique(df['author_id'].values))
# labelencoder = LabelEncoder()
# bridge_df['Bridge_Types_Cat'] = labelencoder.fit_transform(bridge_df['Bridge_Types'])
# df['author_id'] = df['author_id'].astype('category')
# y = df['author_id'].cat.codes.values
# y = csr_matrix(to_categorical(df['author_id'].values))
encoder = OneHotEncoder(categories = 'auto')
y = encoder.fit_transform(df['author_id'].values.reshape(-1,1)).toarray()
# sY = csr_matrix(y)

# lstm = tf.keras.layers.LSTM(4)
# output = lstm(x)

# outputs = tf.keras.layers.LSTM(units=features, stateful=True, return_sequences=True, input_shape=(None,features))(x_data)
    #units = features because we want to use the outputs as inputs
    #None because we want variable length

# K fold cross valitadion below
# k_fold = KFold(n_splits=10)
# for train_indices, test_indices in k_fold.split(x):
#     #output_shape -> (batch_size, steps, units)
#     x = np.array(x)
#     X_train = x[train_indices]
#     y_train = y[train_indices]
#     X_test = x[test_indices]
#     y_test = y[test_indices]
#     X_train = tf.ragged.constant(X_train)
#     X_test = tf.ragged.constant(X_test)
#     # For ragged tensor , get maximum sequence length
#     max_seq = X_train.bounding_shape()[-1]
#     mdl = tf.keras.Sequential([
#         # Input Layer with shape = [Any,  maximum sequence length]
#         tf.keras.layers.Input(shape=[None, max_seq], batch_size=1500, dtype=tf.float32, ragged=True),
#         tf.keras.layers.LSTM(64),
#         tf.keras.layers.Dense(1, activation='softmax')
#     ])
#     opt = SGD(lr=0.000001)
#     mdl.compile(loss = "categorical_crossentropy", optimizer = opt, metrics=['accuracy'])
#     # CategoricalCrossentropy
#     # mdl.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
#     #               optimizer=tf.keras.optimizers.Adam(1e-4),
#     #               metrics=['accuracy'])
#     mdl.summary()
#     history = mdl.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=100)
# X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=5) # 0.25 x 0.8 = 0.2
X_train = tf.ragged.constant(X_train)
X_test = tf.ragged.constant(X_test)
# For ragged tensor , get maximum sequence length
max_seq = X_train.bounding_shape()[-1]
# mdl = tf.keras.Sequential([
#     # Input Layer with shape = [Any,  maximum sequence length]
#     tf.keras.layers.Input(shape=[None, max_seq], batch_size=10000, dtype=tf.float32, ragged=True),
#     tf.keras.layers.LSTM(50),
#     tf.keras.layers.Dropout(0.2),
#     # tf.keras.layers.LSTM(100),
#     # tf.keras.layers.Dropout(0.2),
#     tf.keras.layers.Dense(3, activation='softmax')
# ])


model = tf.keras.Sequential()

tensor_input = model.add(tf.keras.layers.Input(shape=[None, max_seq], batch_size=5000, dtype=tf.float32, ragged=True))
model.add(tf.keras.layers.LSTM(50))
# model.add(tf.keras.layers.LSTM(50))
# model.add(CuDNNLSTM(50, input_shape=(10000,50)))
# model.add(tf.keras.layers.LSTM(50, activation='relu', dropout=0.2))
model.add(tf.keras.layers.Dense(targets_num, activation='softmax'))

# model.add(Dense(50, input_dim=2, activation='relu', kernel_initializer='he_uniform'))
# model.add(Dense(3, activation='softmax'))
# compile model
opt = SGD(lr=0.0001, momentum=0.9)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
model.summary()
# fit model
history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=50)

# history = model.fit_generator(generator=batch_generator(X_train, y_train, 1000), epochs=50, steps_per_epoch=X_train.shape[0])


# opt = SGD(lr=0.001)
# mdl.compile(loss = "categorical_crossentropy", optimizer = opt, metrics=['categorical_accuracy'])
# # CategoricalCrossentropy
# # mdl.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True),
# #               optimizer=tf.keras.optimizers.Adam(1e-4),
# #               metrics=['accuracy'])
# mdl.summary()
# history = mdl.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=20)
