import os
import numpy as np
import pandas as pd
import tensorflow as tf
import json
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split
from tensorflow import keras
from keras.optimizers import SGD, Adam, RMSprop
from sklearn.model_selection import KFold, cross_val_score
from keras.utils import to_categorical
from scipy.sparse import csr_matrix
from sklearn.preprocessing import OneHotEncoder
from file_operation import list_files, sort_files_list

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

# cpus = tf.config.experimental.list_physical_devices('CPU')
# tf.config.experimental.set_memory_growth(cpus[0], True)

class Classifier(object):
    def __init__(self, hidden_num, targets_num, epoches, learning_rate):
        self.hidden_num = hidden_num
        self.targets_num = targets_num
        self.epoches = epoches
        self.learning_rate = learning_rate
        self.metrics = [keras.metrics.CategoricalAccuracy()]

    def authorsIdEncoder(self, authors_list_path):
        encoder = OneHotEncoder()
        authors_df = pd.read_csv(authors_list_path, nrows=self.targets_num)
        authors_id = authors_df['id'].values
        encoder.fit(authors_id.reshape(-1, 1))
        return encoder

    def extract_json(self, json_row):
        return json.loads(json_row)

    def extract_feature(self, inputs, feature_type):
        json_inputs = [self.extract_json(row) for row in inputs]
        return [row['commom_case'][feature_type] for row in json_inputs]

    def build_model(self, max_seq):
        model = keras.Sequential()
        tensor_input = model.add(keras.layers.Input(shape=[None, max_seq], dtype=tf.float32, ragged=True))
        # model.add(keras.layers.LSTM(self.hidden_num, return_sequences=True))
        # model.add(keras.layers.LSTM(self.hidden_num, return_sequences=True))
        model.add(keras.layers.LSTM(self.hidden_num))
        model.add(keras.layers.Dense(self.targets_num, activation='softmax'))
        opt = RMSprop(learning_rate=self.learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=self.metrics)
        model.summary()
        return model

    def build_stateful_model(self, max_seq):
        model = keras.Sequential()
        tensor_input = model.add(keras.layers.Input(batch_shape=[1, None, max_seq], dtype=tf.float32, ragged=True))
        # model.add(keras.layers.LSTM(self.hidden_num, return_sequences=True))
        model.add(keras.layers.LSTM(self.hidden_num, stateful=True, return_sequences=True))
        model.add(keras.layers.LSTM(self.hidden_num, stateful=True))
        model.add(keras.layers.Dense(self.targets_num, activation='softmax'))
        opt = RMSprop(learning_rate=self.learning_rate)
        model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=self.metrics)
        model.summary()
        return model


    def savePlot(self, x, y, x_label, y_label, file_path):
        plt.plot(x, y)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.savefig(file_path)
        plt.close()

    def saveHistory(self, history, result_path, feature_type, n_gram):
        file_path = f'{result_path}/{self.targets_num}_authors_{feature_type}_{n_gram}_{self.hidden_num}_hidden_{self.epoches}_epoches_{self.learning_rate}_learning_rate.pkl'
        f = open(file_path,"wb")
        pickle.dump(history,f)
        f.close()

    def splitDataset(self, x, y):
        return train_test_split(x, y, test_size=0.25)


    def run(self, result_path, dataset_path, authors_list_path, chunksize, feature_type, n_gram):
        monthly_files = sort_files_list(list_files(dataset_path, '*.csv'))
        y_encoder = self.authorsIdEncoder(authors_list_path)
        model = self.build_stateful_model(1)
        model_loss = []
        model_acc = []
        model_history = {
            'loss': np.zeros(self.epoches),
            'val_loss': np.zeros(self.epoches),
            'categorical_accuracy': np.zeros(self.epoches),
            'val_categorical_accuracy': np.zeros(self.epoches),
            # 'auc': np.zeros(self.epoches),
            # 'precision': np.zeros(self.epoches),
            # 'recall': np.zeros(self.epoches)
        }
        for epoch in range(self.epoches):
            print('Epoch: ' + str(epoch))
            tmp_loss = { 'loss': np.array([]), 'val_loss': np.array([]) }
            tmp_accuracy = { 'categorical_accuracy': np.array([]), 'val_categorical_accuracy': np.array([]) }
            # tmp_auc = np.array([])
            # tmp_precision = np.array([])
            # tmp_recall = np.array([])
            for file in monthly_files:
                for df in pd.read_table(file, chunksize=chunksize, lineterminator='\n'):
                    inputs = self.extract_feature(df[feature_type].values, n_gram)
                    x = [np.array(xi).reshape(-1, 1) for xi in inputs]
                    y = y_encoder.transform(df['author_id'].values.reshape(-1,1)).toarray()
                    X_train, X_test, y_train, y_test = self.splitDataset(x, y)
                    X_train = tf.ragged.constant(X_train)
                    X_test = tf.ragged.constant(X_test)
                    # model.train_on_batch(X_train, y_train)
                    # history = model.test_on_batch(X_test, y_test, return_dict=True)
                    history = model.fit(X_train, y_train, validation_data=(X_test, y_test),
                        validation_batch_size=1, batch_size=1, epochs=1, verbose=True, shuffle=False
                    )
                    history = history.history
                    tmp_loss['loss'] = np.append(tmp_loss['loss'], history['loss'])
                    tmp_loss['val_loss'] = np.append(tmp_loss['val_loss'], history['val_loss'])
                    tmp_accuracy['categorical_accuracy'] = np.append(tmp_accuracy['categorical_accuracy'], history['categorical_accuracy'])
                    tmp_accuracy['val_categorical_accuracy'] = np.append(tmp_accuracy['val_categorical_accuracy'], history['val_categorical_accuracy'])
                    # tmp_auc = np.append(tmp_auc, history['auc'])
                    # tmp_precision = np.append(tmp_precision, history['precision'])
                    # tmp_recall = np.append(tmp_recall, history['recall'])
                model.reset_states()
            model_history['loss'][epoch] = np.mean(tmp_loss['loss'])
            model_history['categorical_accuracy'][epoch] = np.mean(tmp_accuracy['categorical_accuracy'])
            model_history['val_loss'][epoch] = np.mean(tmp_loss['val_loss'])
            model_history['val_categorical_accuracy'][epoch] = np.mean(tmp_accuracy['val_categorical_accuracy'])
            # model_history['auc'][epoch] = np.mean(tmp_auc)
            # model_history['precision'][epoch] = np.mean(tmp_precision)
            # model_history['recall'][epoch] = np.mean(tmp_recall)
            print(f'training accuracy: {model_history["categorical_accuracy"][epoch]} val accuracy: {model_history["val_categorical_accuracy"][epoch]}')
            print(f'training loss: {model_history["loss"][epoch]} val loss: {model_history["val_loss"][epoch]}')
        del model
        # accuracy_file_path = f'{result_path}/accuracy_{self.targets_num}_authors_{feature_type}_{n_gram}_{self.hidden_num}_hidden_{self.epoches}_epoches_{self.learning_rate}_learning_rate.jpg'
        # loss_file_path = f'{result_path}/loss_{self.targets_num}_authors_{feature_type}_{n_gram}_{self.hidden_num}_hidden_{self.epoches}_epoches_{self.learning_rate}_learning_rate.jpg'
        self.saveHistory(model_history, result_path, feature_type, n_gram)
        # self.savePlot(range(len(model_history['accuracy'])), model_history['accuracy'],
        #         'epoches', 'accuracy', accuracy_file_path)
        # self.savePlot(range(len(model_history['loss'])), model_history['loss'],
        #     'epoches', 'loss', loss_file_path)
        return model_history


def readHistory(file_path):
    history = {}
    with open(file_path, 'rb') as file:
        history = pickle.load(file)
    return history


if __name__ == "__main__":
    # classifier = Classifier(hidden_num = 100, targets_num = 10, epoches = 100, learning_rate = 0.001)
    # classifier.run(result_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/Experimento/results/tests/1-lstm-stateful',
    #            dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/10_authors_1000_comments/',
    #            authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
    #            chunksize = 500, feature_type = 'words_feat', n_gram = 'unigram')
    classifier = Classifier(hidden_num = 50, targets_num = 10, epoches = 100, learning_rate = 0.0001)
    classifier.run(result_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/Experimento/results/tests/2-lstm-stateful',
               dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/10_authors_1000_comments/',
               authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
               chunksize = 5000, feature_type = 'words_feat', n_gram = 'unigram')
    classifier = Classifier(hidden_num = 100, targets_num = 10, epoches = 100, learning_rate = 0.0001)
    classifier.run(result_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/Experimento/results/tests/2-lstm-stateful',
               dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/10_authors_1000_comments/',
               authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
               chunksize = 5000, feature_type = 'words_feat', n_gram = 'unigram')
    classifier = Classifier(hidden_num = 200, targets_num = 10, epoches = 100, learning_rate = 0.0001)
    classifier.run(result_path = '/home/guilhermecasimiro/SI-Folder/Mestrado/Experimento/results/tests/2-lstm-stateful',
               dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/10_authors_1000_comments/',
               authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
               chunksize = 5000, feature_type = 'words_feat', n_gram = 'unigram')
