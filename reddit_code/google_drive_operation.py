from file_operation import sort_files_list, list_files, retrieve_mime_type
from feature_extraction import extract_comments_n_authors, create_csv_dataset
from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth
import os

class GoogleDriveOperation(object):
    __instance = None
    @staticmethod
    def getInstance():
        """ Static access method. """
        if GoogleDriveOperation.__instance == None:
            GoogleDriveOperation()
        return GoogleDriveOperation.__instance


    def __init__(self):
        if GoogleDriveOperation.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            self.gauth = GoogleAuth()
            self.gauth.CommandLineAuth()
            self.drive = GoogleDrive(self.gauth)
            GoogleDriveOperation.__instance = self


    # parent_id = '1fAU6hJQThLe9kDYRRL-U_pktYwfaqD42' => database_dump/
    def create_folder(self, folder_name, parent_id):
        folder = self.drive.CreateFile({
            'title': folder_name,
            'parents': [{ 'id': parent_id }],
            'mimeType': 'application/vnd.google-apps.folder'
        })
        folder.Upload()
        return folder['id']

    # folder_id = '1fAU6hJQThLe9kDYRRL-U_pktYwfaqD42' => database_dump/
    def list_folder(self, folder_id):
        file_list = self.drive.ListFile({'q': "'%s' in parents and trashed=false" % folder_id}).GetList()
        return file_list


    def upload_file(self, file_path, parent_id):
        file_name = file_path.split('/')[-1]
        file = self.drive.CreateFile({
            'title': file_name,
            'parents': [{ 'id': parent_id }],
            'mimeType': retrieve_mime_type(file_path)
        })
        file.SetContentFile(file_path)
        file.Upload()
        return file

    # gdrive.upload_files('RC_2011-11', '1fAU6hJQThLe9kDYRRL-U_pktYwfaqD42',
    #                     '/home/guilherme/projeto/reddit_thesis/csv/database_dump/',
    #                     'RC_2011-11_*.csv.gz')
    def upload_files(self, folder_name, parent_id, files_path, files_regex):
        # The upload files flow will be:
        # 1. Sorted list of local files to be uploaded.
        sorted_file_list = sort_files_list(list_files(files_path, files_regex))
        # 2. Create a sub-folder in based parent folder id;
        # 3. Get the recent sub-folder id;
        folder_id = self.create_folder(folder_name, parent_id)
        # 4. Upload files to this sub-folder;
        for file_path in sorted_file_list:
            self.upload_file(file_path, folder_id)
        # 5. If everything went okay, delete local files. (optional)

# 1MuFZCOaBem48-W3D-tJmX1qNQmZtTDpU

    def download_file(self, download_path, filename, file_id):
        file = self.drive.CreateFile({ 'id': file_id })
        file.GetContentFile(download_path + filename)
        return download_path + filename

    # gdrive.list_files_in_folder('/home/guilherme/projeto/reddit_thesis/csv/authors_ranking_v2_final.csv',
    #                              10000, '/home/guilherme/projeto/reddit_thesis/csv/database_dump/RC_2009-02/',
    #                              '1-VoEXjb7ImdhLFwx253MSOSWPWmjlUM-')
    def list_files_in_folder(self, authors_ranking_path, chunksize, download_path, folder_id, n = 10000):
        files_list = self.list_folder(folder_id)
        header_fields = ['author_id', 'subreddit_id', 'body', 'created_at', 'reddit_id', 'words_feat',
                         'chars_feat', 'pos_tag_feat']
        new_filename = download_path.split('/')[-2]
        new_comment_file_path = download_path + new_filename + '_10k_authors' + '.csv.gz'
        create_csv_dataset(new_comment_file_path, header_fields)
        processed_count = 0
        total_to_process = len(files_list)
        for gdrive_file in files_list:
            processed_count += 1
            print('Processing: ' + str(processed_count) + ' of ' + str(total_to_process))
            filename = gdrive_file['title']
            file_id = gdrive_file['id']
            comment_file_path = self.download_file(download_path, filename, file_id)
            # extract n authors from comments
            extract_comments_n_authors(authors_ranking_path, comment_file_path, new_comment_file_path,
                                       chunksize, header_fields, n)
            os.remove(comment_file_path)
