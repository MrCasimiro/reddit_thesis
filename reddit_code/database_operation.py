import psycopg2
import psycopg2.extras
import json
import time
import sys
import codecs

# It creates a connection with the db
def connect(dbname, user, password):
    conn = None
    try:
        conn = psycopg2.connect(dbname=dbname, user=user, password=password)
    except Exception as ex:
        print(ex.args)
    return conn

def create_subreddit_table(conn):
    cur = conn.cursor()
    cur.execute(""" CREATE TABLE subreddits (
                    ID      serial PRIMARY KEY,
                    NAME    VARCHAR UNIQUE);""")
    cur.close()
    conn.commit()


def create_author_table(conn):
    cur = conn.cursor()
    cur.execute(""" CREATE TABLE authors (
                    ID      serial PRIMARY KEY,
                    NAME    VARCHAR UNIQUE);""")
    cur.close()
    conn.commit()


def create_comment_table(conn):
    cur = conn.cursor()
    cur.execute(""" CREATE TABLE comments (
                    ID              serial          PRIMARY KEY,
                    SUBREDDIT_ID    BIGINT          REFERENCES SUBREDDITS(ID)   NOT NULL,
                    AUTHOR_ID       BIGINT          REFERENCES AUTHORS(ID)      NOT NULL,
                    BODY            TEXT            NOT NULL,
                    CREATED_AT      TIMESTAMP WITHOUT TIME ZONE    NOT NULL,
                    REDDIT_ID       VARCHAR          UNIQUE NOT NULL,
                    WORDS_FEAT      JSONB           NOT NULL,
                    CHARS_FEAT      JSONB           NOT NULL,
                    POS_TAG_FEAT    JSONB           NOT NULL);""")
    cur.close()
    conn.commit()


# It creates tables for the database
def create_tables(conn):
    create_subreddit_table(conn)
    create_author_table(conn)
    create_comment_table(conn)

