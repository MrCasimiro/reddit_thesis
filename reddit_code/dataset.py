import glob
import os
import pandas as pd
from csvsort import csvsort
from file_operation import list_files, sort_files_list

def authorsId(authors_list_path, authors_num):
    authors_df = pd.read_csv(authors_list_path, nrows=authors_num)
    return authors_df['id'].values

# files_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/10k_authors/'
# dataset_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/datasets/'
# authors_list_path = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv'

# monthly_files = sort_files_list(list_files(dataset_path, '*.csv.gz'))

# authors_num = 10 # make it as parameter
# comments_per_author = 1000 # make it as parameter
# accumulate_comments = True # make it as parameter
# chunksize = 10000
# file = monthly_files[0]
# authors_id = authorsId(authors_list_path, authors_num)
# month_df = pd.read_csv(file, nrows=3000, sep='\t', lineterminator='\n')

# for month_df in pd.read_table(file, chunksize=chunksize):
#     month_df_filtered = month_df[month_df['author_id'].isin(authors_id)]


# header_fields = ['author_id', 'subreddit_id', 'body', 'created_at', 'reddit_id', 'words_feat',
#                  'chars_feat', 'pos_tag_feat']

def select_authors(df, authors_comments, author_id):
    comments_left = authors_comments[author_id]
    author_df = df[df['author_id'] == author_id]
    if(comments_left == 0 or len(author_df) == 0):
        return author_df.sample(0)
    author_df_rows = len(author_df)
    comments_to_pick = comments_left if author_df_rows >= comments_left else author_df_rows
    authors_comments[author_id] = comments_left - comments_to_pick
    return author_df.sample(comments_to_pick)

# generate_datasets(files_path, dataset_path, authors_list_path, 10, 12, False, 10000)
def generate_datasets(files_path, dataset_path, authors_list_path,
                      authors_num, comments_per_author, accumulate_comments,
                      chunksize):
    authors_id = authorsId(authors_list_path, authors_num)
    monthly_files = sort_files_list(list_files(files_path, '*.csv.gz'))
    header_fields = ['author_id', 'created_at', 'words_feat', 'chars_feat', 'pos_tag_feat']
    for file in monthly_files:
        print(file)
        dataset_filename = file.split('/')[-1].split('.gz')[0]
        filtered_df = pd.DataFrame()
        authors_comments = {}
        # populate authors_comments
        list(map(lambda id: authors_comments.update({id : comments_per_author}), authors_id))
        for month_df in pd.read_table(file, sep='\t', chunksize=chunksize, lineterminator='\n',
                                      usecols=header_fields):
            month_df_filtered = month_df[month_df['author_id'].isin(authors_id)]
            print('found ' + str(len(month_df_filtered)) + ' comments filtered')
            if(comments_per_author <= 0 ):
                filtered_df = month_df_filtered
            else:
                for author_id in authors_id:
                    author_df = select_authors(month_df_filtered, authors_comments, author_id)
                    filtered_df = pd.concat([filtered_df, author_df])
        if(filtered_df.empty):
            print('empty month file\n')
            continue
        with open(dataset_path + dataset_filename, 'w') as f:
            f.write(filtered_df.to_csv(index=False, header=True, sep='\t', line_terminator='\n'))
        # TODO: order csv
        csvsort(dataset_path + dataset_filename, [3], delimiter='\t')


# 10 authors
# generate_datasets(files_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/10k_authors/',
#                   dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/10_authors/',
#                   authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
#                   authors_num=10, comments_per_author=0, accumulate_comments=False, chunksize=50000)

# generate_datasets(files_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/csv/database_dump/',
#                   dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/10_authors/',
#                   authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
#                   authors_num=10, comments_per_author=0, accumulate_comments=False, chunksize=50000)

# 50 authors
generate_datasets(files_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/10k_authors/',
                  dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/50_authors/',
                  authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
                  authors_num=50, comments_per_author=0, accumulate_comments=False, chunksize=50000)

generate_datasets(files_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/csv/database_dump/',
                  dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/50_authors/',
                  authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
                  authors_num=50, comments_per_author=0, accumulate_comments=False, chunksize=50000)

# 100 authors
generate_datasets(files_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/10k_authors/',
                  dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/100_authors/',
                  authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
                  authors_num=100, comments_per_author=0, accumulate_comments=False, chunksize=50000)

generate_datasets(files_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/csv/database_dump/',
                  dataset_path='/home/guilhermecasimiro/Mestrado/reddit_thesis/datasets/100_authors/',
                  authors_list_path='/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/authors_ranking_v2_final.csv',
                  authors_num=100, comments_per_author=0, accumulate_comments=False, chunksize=50000)
