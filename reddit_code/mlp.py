""" Multi Layer Perceptron
Author: Guilherme Casimiro """

import numpy as np
import matplotlib.pyplot as plt
from math import log
from scipy import sparse
from scipy.sparse import coo_matrix, hstack
from scipy.sparse import random
from scipy.special import softmax


class MLP(object):
    """docstring for Perceptron"""
    def __init__(self, input_num, h_num, output_num, alpha, classes):
        self.alpha = alpha # taxa de aprendizado
        self.i_weights = random(input_num + 1, h_num + 1) # pesos da camada de entrada
        self.o_weights = random(h_num + 1, output_num) # pesos da camada oculta
        self.loss_ = 0 # erro interno da mlp
        self.one_hot_encode = {} # encodamento da saída para problema multiclasse
        for index in range(len(np.unique(classes))):
            self.one_hot_encode[classes[index]] = index

    # Função Sigmoid
    def sigmoid(self, x):
        exp = np.exp(-x.toarray())
        return sparse.csr_matrix(1 / (1 + exp))

    # Derivada da Sigmoid
    def sigmoid_der(self, x):
        return self.sigmoid(x).multiply(1 - self.sigmoid(x).todense())

    # Etapa Feedforward da MLP
    def feedforward(self, x):
        self.h_layer = self.sigmoid(x.dot(self.i_weights))
        output = sparse.csr_matrix(softmax(self.h_layer.dot(self.o_weights).todense(), axis=1))
        return output

    # Etapa de cálculo dos gradientes dos pesos da camada de entrada e oculta
    # na fase da retropropagação do erro
    def backpropagation(self, X, error):
        d_o_weights = self.h_layer.T.dot(error)
        d_i_weights = X.T.dot((error.dot(self.o_weights.T)).multiply(self.sigmoid_der(self.h_layer)))
        return d_o_weights, d_i_weights

    # Etapa de atualização dos pesos da MLP com base no gradiente calculado
    def backpropagation_weights_update(self, X, preds, error):
        d_o_weights, d_i_weights = self.backpropagation(X, error)
        self.i_weights = sparse.csr_matrix(self.i_weights.todense() - sparse.csr_matrix(d_i_weights.todense() / preds.shape[0]).multiply(self.alpha).todense())
        self.o_weights = sparse.csr_matrix(self.o_weights.todense() - sparse.csr_matrix(d_o_weights / preds.shape[0]).multiply(self.alpha).todense())
        return error

    # Função de perda: Entropia cruzada
    def cross_entropy(self, y, preds):
        sum_score = 0.0
        for i in range(y.shape[0]):
            for j in range(y.shape[1]):
                sum_score += y[i][j] * log(1e-15 + preds[i][j])
        mean_sum_score = 1.0 / y.shape[0] * sum_score
        return -mean_sum_score

    # Etapa de atualização do erro interno da mlp com base na entropia cruzada
    def update_loss(self, y, preds):
        self.loss_ += self.cross_entropy(y, preds)

    # Etapa de preparo da entrada (batch_x) e da saída desejada (batch_y).
    # Para a entrada é adicionado o Bias.
    # Para a saída é encodado com forma no one hot encode definido na inicialização da rede.
    def prepare_x_y(self, batch_x, batch_y):
        bias = sparse.csr_matrix(np.ones((batch_x.shape[0], 1)))
        batch_x = hstack([bias, batch_x])
        batch_y_encoder = np.zeros((len(batch_y), len(self.one_hot_encode.keys())))
        for index in range(len(batch_y)):
            one_hot_position = self.one_hot_encode[batch_y[index]]
            batch_y_encoder[index][one_hot_position] = 1
        return batch_x, batch_y_encoder

    # Etapa de aprendizado incremental.
    # O Batch de treino realiza uma única iteração na rede.
    def partial_fit(self, X, y):
        batch_x, batch_y = self.prepare_x_y(X, y)
        preds = self.feedforward(batch_x)
        error = sparse.csr_matrix(preds.todense() - batch_y)
        self.backpropagation_weights_update(batch_x, preds, error)
        self.update_loss(batch_y, preds.toarray())

    # Etapa de teste.
    # Realiza o cálculo com base na função de entropia cruzada.
    def predict(self, batch_x, batch_y):
        batch_x, batch_y = self.prepare_x_y(batch_x, batch_y)
        test_h_layer = self.sigmoid(batch_x.dot(self.i_weights))
        preds = sparse.csr_matrix(softmax(test_h_layer.dot(self.o_weights).todense(), axis=1))
        label_list = np.fromiter(self.one_hot_encode.keys(), dtype='<U16')
        preds_label = [label_list[pred] for pred in preds.toarray().argmax(axis=1)]
        return np.array(preds_label), self.cross_entropy(batch_y, preds.toarray())
