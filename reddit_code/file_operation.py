import shutil
import glob
import magic
import gzip
import pandas as pd
from tkinter import Tcl

def sort_files_list(file_list):
    return Tcl().call('lsort', '-dict', file_list)

# path = "/run/media/reddit_hd/Mestrado/database_dump/RC_2011-01/*.csv.gz"
# final_path = "/run/media/reddit_hd/Mestrado/database_dump/RC_2011-01/RC_2011-01.csv.gz"
def concatenate_files(path, final_path):
    files = [f for f in glob.glob(path, recursive=True)]
    ordered_files = sort_files_list(files)
    with open(final_path, 'wb') as wfp:
      for fn in ordered_files:
        print(fn)
        with open(fn, 'rb') as rfp:
          shutil.copyfileobj(rfp, wfp)


# path = '/home/guilherme/projeto/reddit_thesis/csv/database_dump/'
# file_regex = 'RC_2011-10_*.csv.gz'
def list_files(path, file_regex):
    return [f for f in glob.glob(path + file_regex, recursive=True)]


def retrieve_mime_type(file_path):
    mime = magic.Magic(mime=True)
    return mime.from_file(file_path)

# Create dataset csv with only headers
def create_csv_dataset(filename, fieldnames):
    gzip.open(filename, 'w').write(('\t'.join(fieldnames) + '\n').encode())


# file_path_read = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/database_dump/RC_2011-01/RC_2011-01.csv.gz'
# file_path_write = '/run/media/guilhermecasimiro/Seagate Backup Plus Drive/Mestrado/database_dump/RC_2011-01/RC_2011-01_new.csv.gz'
def drop_duplicate_rows(file_path_read, file_path_write):
    reddit_id_hash = dict()
    header_fields = ['author_id', 'subreddit_id', 'body', 'created_at', 'reddit_id', 'words_feat',
                     'chars_feat', 'pos_tag_feat']
    create_csv_dataset(file_path_write, header_fields)
    for df in pd.read_table(file_path_read, encoding='utf-8', chunksize=15000):
        # this will drop the duplicated reddit_id in current chunk
        result_df = df.drop_duplicates(subset=['reddit_id'], keep='first')

        # drop rows with reddit_id already stored in reddit_id_hash
        result_df = result_df[~result_df['reddit_id'].apply(lambda reddit_id: reddit_id_hash.get(reddit_id) == 1)]
        repeated_rows = len(df.index) - len(result_df.index)
        if repeated_rows > 0:
            print('Found ' + str(repeated_rows) + ' repeated rows')
        else:
            print('No repeated rows founded')
        # at this point the current chunk is clean from repeated reddit_ids
        result_df['reddit_id'].apply(lambda reddit_id: reddit_id_hash.update({ reddit_id: 1 }))
        #with gzip.open(file_path_write, 'a') as f:
        #    f.write(result_df.to_csv(index=False, header=False, sep='\t', line_terminator='\n').encode())

