import numpy as np
import string
import re
import glob
import csv
import pandas as pd
import json
import gzip
import multiprocessing as mp
import logging
from database_operation import connect
from nltk import word_tokenize, pos_tag
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from datetime import datetime, timezone
from functools import partial


# Pontuaçãos a serem consideradas relevantes pro trabalho.
PUNCTUATIONS_NLTK = "!``''#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~..."

# Word feature extractor
LOWER_WORD_UNIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(1,1), analyzer='word', lowercase=True)
LOWER_WORD_BIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(2,2), analyzer='word', lowercase=True)
LOWER_WORD_TRIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,3), analyzer='word', lowercase=True)
LOWER_WORD_ONE_THREE_GRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(1,3), analyzer='word', lowercase=True)

WORD_UNIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(1,1), analyzer='word', lowercase=False)
WORD_BIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(2,2), analyzer='word', lowercase=False)
WORD_TRIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,3), analyzer='word', lowercase=False)
WORD_ONE_THREE_GRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(1,3), analyzer='word', lowercase=False)

# Char feature extractor
LOWER_CHAR_TRIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,3), analyzer='char', lowercase=True)
LOWER_CHAR_TETRAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(4,4), analyzer='char', lowercase=True)
LOWER_CHAR_PENTAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(5,5), analyzer='char', lowercase=True)
LOWER_CHAR_THREE_FIVE_GRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,5), analyzer='char', lowercase=True)

CHAR_TRIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,3), analyzer='char', lowercase=False)
CHAR_TETRAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(4,4), analyzer='char', lowercase=False)
CHAR_PENTAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(5,5), analyzer='char', lowercase=False)
CHAR_THREE_FIVE_GRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,5), analyzer='char', lowercase=False)

# Pos Tag feature extractor
POS_TAG_TRIGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,3), analyzer='word', lowercase=True)
POS_TAG_TETRAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(4,4), analyzer='word', lowercase=True)
POS_TAG_PENTAGRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(5,5), analyzer='word', lowercase=True)
POS_TAG_THREE_FIVE_GRAM_EXTRACTOR = TfidfVectorizer(ngram_range=(3,5), analyzer='word', lowercase=True)

# Extração de pontuações de um dado comentário
def extract_punctuations(comment):
    comment = comment.replace("'", ' ')
    tokens = word_tokenize(comment.lower())
    return [token for token in tokens if token in PUNCTUATIONS_NLTK]

# Extração de Stopwords (com base nas stopwords definidas na base do NLTK) de um dado comentário
def extract_stopwords(comment):
    comment = comment.replace("'", ' ')
    tokens = word_tokenize(comment.lower())
    return [word for word in tokens if word in stopwords.words('english')]

# Extração de palavras não stopwords de um dado comentário
def extract_words(comment):
    comment = comment.replace("'", ' ')
    comment = re.sub(r'\d+', '', comment)
    tokens = word_tokenize(comment.lower())
    return [word for word in tokens if word not in stopwords.words('english') and word not in PUNCTUATIONS_NLTK]

def select_authors(conn):
    cur = conn.cursor()
    cur.execute("""SELECT * FROM authors""")
    return cur.fetchall()

def select_subreddits(conn):
    cur = conn.cursor()
    cur.execute("""SELECT * FROM subreddits""")
    return cur.fetchall()


# It receives a comment string and output the features as json:
# {
#     'lowercase': {
#         'unigram': [],
#         'bigram': [],
#         'trigram': [],
#         'one_to_three_gram': []
#     }, 'commom_case': {
#         'unigram': [],
#         'bigram': [],
#         'trigram': [],
#         'one_to_three_gram': []
#     }
# }
def extract_word_features(comments):
    uni_lower = LOWER_WORD_UNIGRAM_EXTRACTOR.fit_transform(comments)
    bi_lower = LOWER_WORD_BIGRAM_EXTRACTOR.fit_transform(comments)
    tri_lower = LOWER_WORD_TRIGRAM_EXTRACTOR.fit_transform(comments)
    one_to_three_lower = LOWER_WORD_ONE_THREE_GRAM_EXTRACTOR.fit_transform(comments)
    unigram = WORD_UNIGRAM_EXTRACTOR.fit_transform(comments)
    bigram = WORD_BIGRAM_EXTRACTOR.fit_transform(comments)
    trigram = WORD_TRIGRAM_EXTRACTOR.fit_transform(comments)
    one_to_three_gram = WORD_ONE_THREE_GRAM_EXTRACTOR.fit_transform(comments)

    return [
        {
            'lowercase': {
                'unigram': list(uni_lower[index].data),
                'bigram': list(bi_lower[index].data),
                'trigram': list(tri_lower[index].data),
                'one_to_three_gram': list(one_to_three_lower[index].data)
            }, 'commom_case': {
                'unigram': list(unigram[index].data),
                'bigram': list(bigram[index].data),
                'trigram': list(trigram[index].data),
                'one_to_three_gram': list(one_to_three_gram[index].data)
            }
        }
        for index in range(uni_lower.shape[0])
    ]


# It receives a comment string and output the features as json:
# {
#     'lowercase': {
#         'trigram': [],
#         'tetragram': [],
#         'pentagram': [],
#         'three_to_five_gram': []
#     }, 'commom_case': {
#         'trigram': [],
#         'tetragram': [],
#         'pentagram': [],
#         'three_to_five_gram': []
#     }
# }
def extract_char_features(comments):
    tri_lower = LOWER_CHAR_TRIGRAM_EXTRACTOR.fit_transform(comments)
    tetra_lower = LOWER_CHAR_TETRAGRAM_EXTRACTOR.fit_transform(comments)
    penta_lower = LOWER_CHAR_PENTAGRAM_EXTRACTOR.fit_transform(comments)
    three_to_five_lower = LOWER_CHAR_THREE_FIVE_GRAM_EXTRACTOR.fit_transform(comments)
    trigram = CHAR_TRIGRAM_EXTRACTOR.fit_transform(comments)
    tetragram = CHAR_TETRAGRAM_EXTRACTOR.fit_transform(comments)
    pentagram = CHAR_PENTAGRAM_EXTRACTOR.fit_transform(comments)
    three_to_five_gram = CHAR_THREE_FIVE_GRAM_EXTRACTOR.fit_transform(comments)

    return [
        {
            'lowercase': {
                'trigram': list(tri_lower[index].data),
                'tetragram': list(tetra_lower[index].data),
                'pentagram': list(penta_lower[index].data),
                'three_to_five_gram': list(three_to_five_lower[index].data)
            }, 'commom_case': {
                'trigram': list(trigram[index].data),
                'tetragram': list(tetragram[index].data),
                'pentagram': list(pentagram[index].data),
                'three_to_five_gram': list(three_to_five_gram[index].data)
            }
        }
        for index in range(tri_lower.shape[0])
    ]


# It receives a comment string and convert to pos tag format:
# Input: 'Agent Smith, I presume?'
# Output: 'NNP NNP, PRP VBP.'
def translate_comment_to_pos_tag(comment):
    try:
        pos_tag_tuple = pos_tag(word_tokenize(comment))
        pos_tag_list = dict(pos_tag_tuple)
        replacement = dict((re.escape(k), v) for k, v in pos_tag_list.items())
        pattern = re.compile('|'.join(replacement.keys()))
        return pattern.sub(lambda m: replacement[re.escape(m.group(0))], comment)
    except KeyError:
        logging.info(comment)
        logging.info(pos_tag_list)
        return ''


# It receives a comment string and output the features as json:
# {
#     'lowercase': {
#         'trigram': [],
#         'tetragram': [],
#         'pentagram': [],
#         'three_to_five_gram': []
#     }
# }
def extract_pos_tag_features(comments):
    pos_tag_comments = list(map(translate_comment_to_pos_tag, comments))

    trigram = POS_TAG_TRIGRAM_EXTRACTOR.fit_transform(pos_tag_comments)
    tetragram = POS_TAG_TETRAGRAM_EXTRACTOR.fit_transform(pos_tag_comments)
    pentagram = POS_TAG_PENTAGRAM_EXTRACTOR.fit_transform(pos_tag_comments)
    three_to_five_gram = POS_TAG_THREE_FIVE_GRAM_EXTRACTOR.fit_transform(pos_tag_comments)

    return [
        {
            'lowercase': {
                'trigram': list(trigram[index].data),
                'tetragram': list(tetragram[index].data),
                'pentagram': list(pentagram[index].data),
                'three_to_five_gram': list(three_to_five_gram[index].data)
            }
        }
        for index in range(trigram.shape[0])
    ]

# Create dataset csv with only headers
def create_csv_dataset(filename, fieldnames):
    gzip.open(filename, 'w').write(('\t'.join(fieldnames) + '\n').encode())

#def create_csv_dataset(filename, fieldnames):
#    open(filename, 'w').write('\t'.join(fieldnames) + '\n')


def string_to_timestamp(date_string):
    try:
        return datetime.fromtimestamp(int(date_string), tz=timezone.utc)
    except ValueError:
        return 0

def merge_and_sum(dict_1, dict_2):
    return { k: dict_1.get(k, 0) + dict_2.get(k, 0) for k in set(dict_1) | set(dict_2) }


def authors_ranking(filtered_path):
    general_authors_ranking = dict()
    files = [f for f in glob.glob(filtered_path + "*.csv", recursive=True)]
    files.sort()
    for filename in files:
        print('New group: ' + filename + '\n')
        for df in pd.read_csv(filename, chunksize=100000, lineterminator='\n'):
            chunk_authors_ranking = df.groupby(['author']).size().to_dict()
            general_authors_ranking = merge_and_sum(general_authors_ranking, chunk_authors_ranking)
            print(len(general_authors_ranking))
    return general_authors_ranking


# create_comment_csv('/home/guilherme/projeto/reddit_thesis/csv', 3500, 14)
def create_comment_csv(authors_ranking_path, n=10000,
                       data_path='/run/media/reddit_hd/Mestrado', chunksize=10000, processes_num=12,
                       db_name='guilhermecasimiro', db_pass=''):
    author_df = pd.read_csv(authors_ranking_path, nrows=n)
    logging.basicConfig(filename='../comments-extract.log', level=logging.INFO)
    conn = connect('reddit_comments', db_name, db_pass)
    files = [f for f in glob.glob(data_path + "/filtered/*.csv", recursive=True)]
    files.sort()
    authors_hash = dict((name, id) for id, name in select_authors(conn))
    subreddits_hash = dict((name, id) for id, name in select_subreddits(conn))
    for filename in files:
        destination_path = data_path + '/database_dump/'
        #dest_file = destination_path + filename.split('/')[-1].split('.csv')[0] + '.gz'
        #dest_file = destination_path + filename.split('/')[-1]
        dest_file_path = destination_path + filename.split('/')[-1].split('.csv')[0]
        header_fields = ['author_id', 'subreddit_id', 'body', 'created_at', 'reddit_id', 'words_feat',
                         'chars_feat', 'pos_tag_feat']
        chunks_num = 1
        #dest_file = dest_file_path + '_' + str(chunks_num) + '.csv' + '.gz'
        dest_file = dest_file_path + '_' + str(chunks_num)
        create_csv_dataset(dest_file + '.csv' + '.gz', header_fields)
        logging.info('Start filter CSV for: ' + filename + '\n')
        pool = mp.Pool(processes_num)
        for df in pd.read_csv(filename, chunksize=chunksize*processes_num, lineterminator='\n'):
            logging.info('New Chunk - Filtered: ' + str(df.shape[0]) + ' comments\n')
            filtered_df = df[df['author'].apply(lambda name: authors_hash[name]).isin(author_df['id'].values)]
            logging.info(filtered_df.head())
            #filtered_df = df[df['author_id'].isin(author_df['id'].values)]
            logging.info('Filtered: ' + str(len(filtered_df.index)) + ' of ' + str(len(df.index)))
            df_split = np.array_split(filtered_df, processes_num)
            func = partial(extract_features, header_fields, dest_file, authors_hash, subreddits_hash)
            logging.info('Processing Chunk')
            results = pool.map(func, df_split)
            logging.info('Finish chunk')
            #processed_df = pd.concat(results)
            logging.info('Write chunk')
            #with gzip.open(dest_file, 'a') as f:
            #    f.write(processed_df.to_csv(index=False, header=False, sep='\t', line_terminator='\n').encode())
            chunks_num += 1
            dest_file = dest_file_path + '_' + str(chunks_num)
            #dest_file = dest_file_path + '_' + str(chunks_num) + '.csv' + '.gz'
            #gzip.open(dest_file, 'a').close()
        pool.close()
        pool.join()

def extract_features(header_fields, dest_file, authors_hash, subreddits_hash, df):
    import os
    reddit_df = pd.DataFrame(columns=header_fields)
    comments = df['comment'].apply(lambda comment: comment.replace('\n', ' ')).values
    reddit_df['author_id'] = df['author'].apply(lambda name: authors_hash[name])
    reddit_df['subreddit_id'] = df['subreddit'].apply(lambda name: subreddits_hash[name])
    reddit_df['body'] = comments
    reddit_df['created_at'] = df['date'].apply(string_to_timestamp)
    reddit_df['reddit_id'] = df['id'].values
    reddit_df['words_feat'] = extract_word_features(comments)
    reddit_df['chars_feat'] = extract_char_features(comments)
    reddit_df['pos_tag_feat'] = extract_pos_tag_features(comments)
    reddit_df['words_feat'] = reddit_df['words_feat'].apply(json.dumps)
    reddit_df['chars_feat'] = reddit_df['chars_feat'].apply(json.dumps)
    reddit_df['pos_tag_feat'] = reddit_df['pos_tag_feat'].apply(json.dumps)
    valid_df = reddit_df[~reddit_df['created_at'].apply(invalid_timestamp) & ~reddit_df['pos_tag_feat'].apply(invalid_pos_tag_feat) & ~reddit_df['words_feat'].apply(invalid_word_feat) & ~reddit_df['chars_feat'].apply(invalid_char_feat)]
    #return valid_df
    # valid_df['body'] = valid_df['body'].replace('\n', ' ')
    mp_pid = os.getpid()
    with gzip.open(dest_file + '_' + str(mp_pid) + '.csv' + '.gz', 'w') as f:
        f.write(valid_df.to_csv(index=False, header=False, sep='\t', line_terminator='\n').encode())
        #valid_df.to_csv(f, index=False, header=False, sep='\t', line_terminator='\n')


def invalid_pos_tag_feat(pos_tag_feat):
    pos_tag_json = json.loads(pos_tag_feat)
    if len(pos_tag_json['lowercase']['trigram']) == 0: return True
    if len(pos_tag_json['lowercase']['tetragram']) == 0: return True
    if len(pos_tag_json['lowercase']['pentagram']) == 0: return True
    if len(pos_tag_json['lowercase']['three_to_five_gram']) == 0: return True
    return False


def invalid_word_feat(word_feat):
    word_feat_json = json.loads(word_feat)
    if len(word_feat_json['lowercase']['unigram']) == 0: return True
    if len(word_feat_json['lowercase']['bigram']) == 0: return True
    if len(word_feat_json['lowercase']['trigram']) == 0: return True
    if len(word_feat_json['lowercase']['one_to_three_gram']) == 0: return True
    if len(word_feat_json['commom_case']['unigram']) == 0: return True
    if len(word_feat_json['commom_case']['bigram']) == 0: return True
    if len(word_feat_json['commom_case']['trigram']) == 0: return True
    if len(word_feat_json['commom_case']['one_to_three_gram']) == 0: return True
    return False


def invalid_char_feat(char_feat):
    char_feat_json = json.loads(char_feat)
    if len(char_feat_json['lowercase']['trigram']) == 0: return True
    if len(char_feat_json['lowercase']['tetragram']) == 0: return True
    if len(char_feat_json['lowercase']['pentagram']) == 0: return True
    if len(char_feat_json['lowercase']['three_to_five_gram']) == 0: return True
    if len(char_feat_json['commom_case']['trigram']) == 0: return True
    if len(char_feat_json['commom_case']['tetragram']) == 0: return True
    if len(char_feat_json['commom_case']['pentagram']) == 0: return True
    if len(char_feat_json['commom_case']['three_to_five_gram']) == 0: return True
    return False


def invalid_timestamp(timestamp):
    if timestamp == 0: return True
    if str(timestamp) == '0':return True
    return False



def generate_authors_name_id(authors_path, authors_ranking_path):
    conn = connect('reddit_authors', 'guilhermecasimiro', '')
    authors_hash = dict((name, id) for id, name in select_authors(conn))
    df = pd.read_csv(authors_path, names=['author', 'comments_num'])
    df['id'] = df['author'].apply(lambda name: authors_hash[name])
    with open(authors_ranking_path, 'w') as f:
        df.to_csv(f, index=False)


def extract_comments_n_authors(authors_ranking_path, comment_file_path, new_comment_file_path, chunksize, header_fields=None, n=10000):
    author_df = pd.read_csv(authors_ranking_path, nrows=n)
    for df in pd.read_csv(comment_file_path, chunksize=chunksize, sep='\t', lineterminator='\n', names=header_fields):
        filtered_df = df[df['author_id'].isin(author_df['id'].values)]
        print('Filtered: ' + str(len(filtered_df.index)) + ' of ' + str(len(df.index)))
        with gzip.open(new_comment_file_path, 'a') as f:
            f.write(filtered_df.to_csv(index=False, header=False, sep='\t', line_terminator='\n').encode())

